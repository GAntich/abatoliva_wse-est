package com.example.pol.weest20;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

public class GridViewImageAdapter extends BaseAdapter {
    Context mContext;
    String activity = "";
    private Integer[] mThumbIds = {
            R.drawable.openbook,
            R.drawable.guia_est,
    };
    private Integer[] mThumbIdsStatistics2 = {
            R.drawable.demografic,
            R.drawable.onepopulation,
            R.drawable.twopopulation,
    };
    private Integer[] mThumbIdsWorldAndSpanishEconomy = {
            R.drawable.contents,
            R.drawable.indicators,
    };
    private Integer[] mThumbIdsWorldAndSpanishEconomyIndicators = {
            R.drawable.leading,
            R.drawable.lagging,
            R.drawable.linechart,
            R.drawable.finance,
    };
    private Integer[] mThumbIdsWorldAndSpanishEconomyConcepts = {
            R.drawable.gdp,
            R.drawable.customer,
            R.drawable.investment,
            R.drawable.industry,
            R.drawable.balance,
            R.drawable.exchangerate,
            R.drawable.money,
    };
    private Integer[] mThumbIdsConcept = {
            R.drawable.estadistica1,
            R.drawable.estadistica2,
    };
    private Integer[] mStringIds = {
            R.string.statistics_concepts, R.string.statistics_guide
    };
    private Integer[] mStringIdsStatistics2 = {
            R.string.statistics2_1, R.string.statistics2_2,R.string.statistics2_3
    };
    private Integer[] mStringIdsWorldAndSpanishEconomy = {
            R.string.worldandspanisheconomy_concepts, R.string.worldandspanisheconomy_indicators
    };
    private Integer[] mStringIdsWorldAndSpanishEconomyIndicators = {
            R.string.worldandspanisheconomy_indicators_leading,
            R.string.worldandspanisheconomy_indicators_lagging,
            R.string.worldandspanisheconomy_indicators_fiscal,
            R.string.worldandspanisheconomy_indicators_finance,
    };
    private Integer[] mStringIdsWorldAndSpanishEconomyConcepts = {
            R.string.worldandspanisheconomy_concepts_gdp,
            R.string.worldandspanisheconomy_concepts_cons,
            R.string.worldandspanisheconomy_concepts_inv,
            R.string.worldandspanisheconomy_concepts_iac,
            R.string.worldandspanisheconomy_concepts_tbop,
            R.string.worldandspanisheconomy_concepts_er,
            R.string.worldandspanisheconomy_concepts_mafm,
    };
    private Integer[] mStringIdsConcept = {
            R.string.statistics_concepts_theme1, R.string.statistics_concepts_theme2
    };

    public GridViewImageAdapter(Context ctx, String activity) {
        this.mContext = ctx;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        switch (activity){
            case "statistics":
                return mThumbIds.length;
            case "worldandspanisheconomy":
                return mThumbIdsWorldAndSpanishEconomy.length;
            case "indicators":
                return mThumbIdsWorldAndSpanishEconomyIndicators.length;
            case "concepts":
                return mThumbIdsConcept.length;
            case "econcepts":
                return mThumbIdsWorldAndSpanishEconomyConcepts.length;
            case "statistics2":
                return mThumbIdsStatistics2.length;
        }
    return mThumbIds.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.griditem, viewGroup, false);
        }
        ImageView imagen_grid = (ImageView) view.findViewById(R.id.imagen_grid);
        TextView string_grid = (TextView) view.findViewById(R.id.string_grid);
        if (activity.equals("statistics")) {
            imagen_grid.setImageResource(mThumbIds[position]);
            string_grid.setText(mStringIds[position]);
        }
        if (activity.equals("statistics2")) {
            imagen_grid.setImageResource(mThumbIdsStatistics2[position]);
            string_grid.setText(mStringIdsStatistics2[position]);
        }
        if (activity.equals("worldandspanisheconomy")){
            imagen_grid.setImageResource(mThumbIdsWorldAndSpanishEconomy[position]);
            string_grid.setText(mStringIdsWorldAndSpanishEconomy[position]);
        }
        if (activity.equals("indicators")){
            imagen_grid.setImageResource(mThumbIdsWorldAndSpanishEconomyIndicators[position]);
            string_grid.setText(mStringIdsWorldAndSpanishEconomyIndicators[position]);
        }
        if (activity.equals("concepts")) {
            imagen_grid.setImageResource(mThumbIdsConcept[position]);
            string_grid.setText(mStringIdsConcept[position]);
        }
        if (activity.equals("econcepts")) {
            imagen_grid.setImageResource(mThumbIdsWorldAndSpanishEconomyConcepts[position]);
            string_grid.setText(mStringIdsWorldAndSpanishEconomyConcepts[position]);
        }
        return view;
    }
}