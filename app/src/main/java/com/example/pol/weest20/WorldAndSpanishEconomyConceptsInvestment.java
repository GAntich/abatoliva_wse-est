package com.example.pol.weest20;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ExpandableListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class WorldAndSpanishEconomyConceptsInvestment extends BaseActivity {

    private ExpandableListView listView;
    private ExpandableListAdapter listAdapter;
    private List<String> listDataHeader;
    private HashMap<String,List<String>> listHash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_world_and_spanish_economy_concepts_investment);

        setTitle("Investment");

        listView = (ExpandableListView)findViewById(R.id.lvExp);
        initData();
        listAdapter = new ExpandableListAdapter(this,listDataHeader,listHash);
        listView.setAdapter(listAdapter);
    }
    private void initData() {
        listDataHeader = new ArrayList<>();
        listHash = new HashMap<>();

        listDataHeader.add("Investment");
        listDataHeader.add("Fixed investment");
        listDataHeader.add("Investment intentions");
        listDataHeader.add("Stocks (inventories)");

        List<String> edmtDev = new ArrayList<>();
        edmtDev.add("Investment deserves special attention because it is so important for the future health of an economy. It lays the basis for future production. Investment is spending on physical assets with a life of more than one year. This should be distinguished from financial transactions which are known as investment in everyday language but which are – from an economic viewpoint – savings. It is conventional to say that businesses invest while individuals consume. If a household buys itself a personal computer, this is recorded in the national accounts as personal consumption. If a business buys the same model, the spending is classed as investment. The rationale is that the household uses a pc for “pleasure” while a business uses it in the production of future output. A company’s stocks of raw materials and goods are classed as investment.");

        List<String> androidStudio = new ArrayList<>();
        androidStudio.add("Fixed investment is spending on physical assets. Total investment is fixed investment plus investment in stocks of raw materials and goods. Physical assets include infrastructure such as roads and docks; buildings such as dwellings, factories and offices; plant and machinery; vehicles; and equipment such as computers. These generally provide the potential for higher output in the future. The economic (as opposed to the social) benefit of dwellings and some infrastructure is more arguable, but most infrastructure boosts economic efficiency. For example, new roads help to get delivery teams back for more work rather than crawling at 10kph through the world’s congested cities.");

        List<String> xamarin = new ArrayList<>();
        xamarin.add("Governments and trade associations such as the US Institute for Supply Management and the Confederation of British Industry publish the results of surveys of investment intentions. These may be wholly subjective (“Do you expect more or fewer capital authorisations over the next 12 months?”) or misleadingly quantitative (“How many dollars’ worth of capital spending will you undertake in the calendar year?”).");

        List<String> uwp = new ArrayList<>();
        uwp.add("Stocks or inventories are materials and fuel, work-in-progress and finished goods held by companies. The book value of stocks changes for two reasons. Stock appreciation is an increase in the money value of stocks owing to inflation. It adds to nominal income (the inventories can be sold at a profit) but there is no addition to real output. Stockbuilding (or destocking) is a change in the physical volume of inventories. It reflects the production of goods and affects nominal and real output. Data are generally collected in value terms and deflated into volume terms using assumptions about accounting practices, stockholding patterns and price changes. The breakdown between the physical change and stock appreciation can be unreliable, especially during periods of rapid inflation.");

        listHash.put(listDataHeader.get(0),edmtDev);
        listHash.put(listDataHeader.get(1),androidStudio);
        listHash.put(listDataHeader.get(2),xamarin);
        listHash.put(listDataHeader.get(3),uwp);
    }
}
