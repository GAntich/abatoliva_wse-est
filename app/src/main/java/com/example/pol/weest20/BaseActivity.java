package com.example.pol.weest20;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
    import android.widget.Toast;



public class BaseActivity extends AppCompatActivity {

    public MyApplication getMyApplication() {
        return ((MyApplication)getApplication());
    }

    public void showToast(String text) {
        Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
    }
}