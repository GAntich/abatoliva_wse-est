package com.example.pol.weest20;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

public class StatisticsGuideSearch extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics_guide_search);

        setTitle("Resultados de búsqueda");

        Intent intent = getIntent();
        String message = intent.getStringExtra(StatisticsGuide.EXTRA_MESSAGE);

        TextView title = (TextView) findViewById(R.id.title);
        TextView description = (TextView) findViewById(R.id.description);
        ImageView image = (ImageView) findViewById(R.id.imageView);

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        switch (message){
            case "onesample_parametric_independent":
                title.setText(R.string.title_onesample_parametric_independent);
                description.setText(R.string.description_onesample_parametric_independent);
                image.setImageResource(R.drawable.tunamuestra);
                break;
            case "onesample_noparametric_independent":
                title.setText(R.string.title_onesample_noparametric_independent);
                description.setText(R.string.description_onesample_noparametric_independent);
                image.setImageResource(R.drawable.image2);
                break;
            case "twosample_parametric_independent":
                title.setText(R.string.title_twosample_parametric_independent);
                description.setText(R.string.description_twosample_parametric_independent);
                image.setImageResource(R.drawable.tindependiente);
                break;
            case "twosample_parametric_related":
                title.setText(R.string.title_twosample_parametric_related);
                description.setText(R.string.description_twosample_parametric_related);
                image.setImageResource(R.drawable.tpareada);
                break;
            case "twosample_noparametric_independent":
                title.setText(R.string.title_twosample_noparametric_independent);
                description.setText(R.string.description_twosample_noparametric_independent);
                image.setImageResource(R.drawable.uman);
                break;
            case "twosample_noparametric_related":
                title.setText(R.string.title_twosample_noparametric_related);
                description.setText(R.string.description_twosample_noparametric_related);
                image.setImageResource(R.drawable.wilcoxon);
                break;
            case "moresample_parametric_independent":
                title.setText(R.string.title_moresample_parametric_independent);
                description.setText(R.string.description_moresample_parametric_independent);
                image.setImageResource(R.drawable.anova);
                break;
            case "moresample_noparametric_independent":
                title.setText(R.string.title_moresample_noparametric_independent);
                description.setText(R.string.description_moresample_noparametric_independent);
                image.setImageResource(R.drawable.kruskal);
                break;
            case "moresample_noparametric_related":
                title.setText(R.string.title_moresample_noparametric_related);
                description.setText(R.string.description_moresample_noparametric_related);
                image.setImageResource(R.drawable.friedman);
                break;
        }

    }
}
