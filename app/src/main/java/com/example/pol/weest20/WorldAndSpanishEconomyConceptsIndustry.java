package com.example.pol.weest20;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ExpandableListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class WorldAndSpanishEconomyConceptsIndustry extends BaseActivity {

    private ExpandableListView listView;
    private ExpandableListAdapter listAdapter;
    private List<String> listDataHeader;
    private HashMap<String,List<String>> listHash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_world_and_spanish_economy_concepts_industry);

        setTitle("Indutry and Commerce");

        listView = (ExpandableListView)findViewById(R.id.lvExp);
        initData();
        listAdapter = new ExpandableListAdapter(this,listDataHeader,listHash);
        listView.setAdapter(listAdapter);
    }
    private void initData() {
        listDataHeader = new ArrayList<>();
        listHash = new HashMap<>();

        listDataHeader.add("Business conditions");
        listDataHeader.add("Industrial and manufacturing production");
        listDataHeader.add("Capacity use and utilization");
        listDataHeader.add("Manufacturing orders");
        listDataHeader.add("Motor vehicles");
        listDataHeader.add("Construction orders and output");
        listDataHeader.add("Wholesale sales or turnover");

        List<String> edmtDev = new ArrayList<>();
        edmtDev.add("Surveys provide valuable evidence of perceptions and expectations relating to business conditions, usually in manufacturing. Responses are subjective but they give early signals of changes in economic trends. Private-sector bodies conduct surveys in some countries (for example, confederations of industry in Australia, Britain, Finland; ifo in Germany), but some government agencies do the work (for example, Statistics Canada, the Bank of Japan). Many surveys are quarterly, although where they are monthly some questions are asked only 3–4 times a year (for example, capacity use in France and Germany).");

        List<String> androidStudio = new ArrayList<>();
        androidStudio.add("There are two main series, which are usually released together. Manufacturing production. This is the value-added output of manufacturing companies. Some countries such as Italy and Spain include mining and quarrying. Industrial production. This is manufacturing production plus the supply of energy and water, and the output of mines, oil wells and quarries, and, usually, construction. It generally excludes agriculture, trade, transport, finance and all other services.");

        List<String> xamarin = new ArrayList<>();
        xamarin.add("Total capacity. Capacity is a vague, hard-to-measure concept which varies over time and according to economic conditions. The term is generally used to refer to the sustainable maximum output that could be produced by existing (installed) manufacturing plant and machinery, although sometimes other factors such as labour are taken into account. Either way “normal capacity” will vary if working practices are changed, even if only by increasing the working week by one hour. Capacity use. Capacity use is usually defined as output divided by sustainable maximum capacity. Sustainable maximum output is lower than temporarily attainable peak production, so use of (sustainable) capacity can and occasionally does exceed 100% in some industries. Overall capacity use does not go as high as 100% because different companies reach their peaks at different stages of the economic cycle. Also bottlenecks in one industry restrict supplies and therefore output in another: in major cyclical peaks, a shortage of metals can limit output of consumer durables and business machinery, and restrain capacity utilisation in those industries. Capital investment. Figures for capital investment are sometimes taken as indicators of changes in capacity, but investment is usually measured gross, with no allowance for scrapping. Even where allowance is made, the net capital stock may not be representative of output potential. Moreover, the increase in capacity derived from a unit of new capital is affected by factors such as new technology, an increasingly shorter life for capital equipment, a longer work week for capital, restructuring of industry and the closure of plants.");

        List<String> uwp = new ArrayList<>();
        uwp.add("Manufacturing orders (including machine tool orders) ripple through the economy. An order for a washing machine may prompt an order for a metal pressing which in turn will provoke an order for sheet steel. Each order will reflect the output price, while manufacturing output and gdp will rise by the value-added component only. Orders are therefore much more volatile than manufacturing production.");

        List<String> asd = new ArrayList<>();
        asd.add("Manufacturer’s production; sales, and registrations of new vehicles with national licensing authorities. Registrations and sales are broadly similar; the difference mainly reflects the method of data collection. The figures usually relate to the number of units, that is, volume rather than value. They may not reveal very much about the pattern of demand for cheap or expensive vehicles or about changes in quality. However, they are a useful indicator of manufacturing production; demand for a durable good which is vulnerable to the economic cycle; competitive pressures, especially between domestic and overseas producers; and import and export trends. ");

        List<String> asdf = new ArrayList<>();
        asdf.add("Construction covers buildings and infrastructure (such as roads and ports). Orders are sometimes based on contracting work. This may include projects such as oil rigs which really belong in manufacturing production under steel fabrication. The figures usually exclude residential dwellings, but this should be checked.");

        List<String> asdfg = new ArrayList<>();
        asdfg.add("Wholesale sales (called wholesale turnover in Germany) are an important link in the supply chain. Wholesalers channel imports and domestically produced or processed goods through to final users. Where stocks and orders are available, these provide a useful check on trends. A fall in wholesale sales or a rise in wholesale inventories suggests or confirms slack in business and retail demand. There are relatively few figures on the services sector, but wholesale sales provide extremely loose indicators of demand from, for example, hotels and catering establishments. They are also indicative of demand for business goods including, in some countries, building materials. Wholesale sales are a reasonable signal of consumer demand.");


        listHash.put(listDataHeader.get(0),edmtDev);
        listHash.put(listDataHeader.get(1),androidStudio);
        listHash.put(listDataHeader.get(2),xamarin);
        listHash.put(listDataHeader.get(3),uwp);
        listHash.put(listDataHeader.get(4),asd);
        listHash.put(listDataHeader.get(5),asdf);
        listHash.put(listDataHeader.get(6),asdfg);
    }
}
