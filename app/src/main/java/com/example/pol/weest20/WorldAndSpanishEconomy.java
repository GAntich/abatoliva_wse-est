package com.example.pol.weest20;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

public class WorldAndSpanishEconomy extends Fragment {

    GridView gridView;

    public static WorldAndSpanishEconomy newInstance() {
        WorldAndSpanishEconomy fragment = new WorldAndSpanishEconomy();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_stadistics, container, false);
        gridView = (GridView) view.findViewById(R.id.gridview);
        gridView.setAdapter(new GridViewImageAdapter(getActivity(), "worldandspanisheconomy"));

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                switch(position){
                    case 0:
                        Intent concepts = new Intent(getActivity(), WorldAndSpanishEconomyConcepts.class);
                        startActivity(concepts);

                        break;
                    case 1:
                        Intent indicators = new Intent(getActivity(), WorldAndSpanishEconomyIndicators.class);
                        startActivity(indicators);

                        break;
                    default:
                        break;
                }
            }
        });


        return view;
    }
}
