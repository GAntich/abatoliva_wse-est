package com.example.pol.weest20;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

public class Statistics extends Fragment {

    GridView gridView;

    public static Statistics newInstance() {
        Statistics fragment = new Statistics();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_stadistics, container, false);
        gridView = (GridView) view.findViewById(R.id.gridview);
        gridView.setAdapter(new GridViewImageAdapter(getActivity(), "statistics"));

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                switch(position){
                    case 0:
                        Intent concepts = new Intent(getActivity(), StatisticsConcepts.class);
                        startActivity(concepts);

                        break;
                    case 1:
                        Intent guide = new Intent(getActivity(), StatisticsGuide.class);
                        startActivity(guide);

                        break;
                    default:
                        break;
                }
            }
        });


        return view;
    }
}
