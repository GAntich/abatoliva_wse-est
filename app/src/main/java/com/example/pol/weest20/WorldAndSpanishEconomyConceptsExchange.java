package com.example.pol.weest20;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ExpandableListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class WorldAndSpanishEconomyConceptsExchange extends BaseActivity {

    private ExpandableListView listView;
    private ExpandableListAdapter listAdapter;
    private List<String> listDataHeader;
    private HashMap<String,List<String>> listHash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_world_and_spanish_economy_concepts_exchange);

        setTitle("Exchange Rate");

        listView = (ExpandableListView)findViewById(R.id.lvExp);
        initData();
        listAdapter = new ExpandableListAdapter(this,listDataHeader,listHash);
        listView.setAdapter(listAdapter);
    }
    private void initData() {
        listDataHeader = new ArrayList<>();
        listHash = new HashMap<>();

        listDataHeader.add("Nominal exchange rate");
        listDataHeader.add("Effective exchange rates");
        listDataHeader.add("Real exchange rates, competitiveness");
        listDataHeader.add("Special drawing rights (SDRs)");

        List<String> edmtDev = new ArrayList<>();
        edmtDev.add("An exchange rate indicates how many units of one currency can be purchased with a single unit of another. For example, a rate of ¥100 against the dollar indicates that one dollar buys 100 yen: $1 ¥100. Stronger and weaker. If the rate changes from $1 ¥100 to $1 ¥200 the dollar has risen or strengthened by 100% against the yen (it will buy 100% more yen). The yen has fallen or weakened against the dollar, but not by 100% or it would be worthless. It has moved from ¥100 $1 to ¥100 $0.50, a fall of 50%. When currencies get stronger or weaker, they are said to have appreciated or depreciated if they are floating-rate currencies or to have been revalued or devalued if their rates are fixed by the central bank.");

        List<String> androidStudio = new ArrayList<>();
        androidStudio.add("An effective exchange rate (eer) measures the overall value of one currency against a basket of other currencies. Changes indicate the average change in one currency relative to all the others. Effective exchange rates are weighted averages of many currency movements with weights chosen to reflect the relative importance of each currency in the home country’s trade. For example, if the dollar appreciates by 10% against the Japanese yen but is unchanged against all other currencies, and if the yen accounts for 25% of American trade, the dollar’s effective exchange rate has risen by 2.5%. For obvious reasons eers are sometimes known as trade-weighted exchange rates. There are many ways of selecting the weights, based on imports of manufactured goods, total trade, and so on.");

        List<String> xamarin = new ArrayList<>();
        xamarin.add("The international competitiveness of goods and services produced in a country depends on relative movements in costs or prices after adjusting for exchange-rate movements. For example, if prices increase by 4% in Germany and 6% in America, American competitiveness appears to have fallen by 2%. However, if over the same period the dollar fell by 3%, overall American competitiveness has actually improved by 1%. Such measures of overall competitiveness are known as “relative costs or prices expressed in a common currency” or, more simply, real exchange rates.");

        List<String> uwp = new ArrayList<>();
        uwp.add("The sdr (special drawing right) has some of the characteristics of a world currency. It was introduced by the imf in 1970 to boost world liquidity after the ratio of world reserves to imports had fallen by half since the 1950s. Through book-keeping entries, the Fund allocated sdrs to member countries in proportion to their quotas. Countries in need of foreign currency may obtain them from other central banks in exchange for sdrs.");

        listHash.put(listDataHeader.get(0),edmtDev);
        listHash.put(listDataHeader.get(1),androidStudio);
        listHash.put(listDataHeader.get(2),xamarin);
        listHash.put(listDataHeader.get(3),uwp);
    }
}
