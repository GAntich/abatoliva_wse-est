package com.example.pol.weest20;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

public class WorldAndSpanishEconomyIndicators extends BaseActivity {

    GridView gridView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_world_and_spanish_economy_indicators);

        setTitle("Indicators");

        gridView = (GridView) findViewById(R.id.gridview);
        gridView.setAdapter(new GridViewImageAdapter(this, "indicators"));

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                switch(position){
                    case 0:
                        Intent leading = new Intent(WorldAndSpanishEconomyIndicators.this, WorldAndSpanishEconomyIndicatorsLeading.class);
                        startActivity(leading);
                        break;
                    case 1:
                        Intent lagging = new Intent(WorldAndSpanishEconomyIndicators.this, WorldAndSpanishEconomyIndicatorsLagging.class);
                        startActivity(lagging);
                        break;
                    case 2:
                        Intent fiscal = new Intent(WorldAndSpanishEconomyIndicators.this, WorldAndSpanishEconomyIndicatorsCoincident.class);
                        startActivity(fiscal);
                        break;
                    case 3:
                        Intent finance = new Intent(WorldAndSpanishEconomyIndicators.this, WorldAndSpanishEconomyIndicatorsFinance.class);
                        startActivity(finance);
                        break;
                    default:
                        break;
                }
            }
        });
    }
}
