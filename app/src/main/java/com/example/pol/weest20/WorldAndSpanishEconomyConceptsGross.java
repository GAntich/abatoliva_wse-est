package com.example.pol.weest20;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ExpandableListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class WorldAndSpanishEconomyConceptsGross extends BaseActivity {

    private ExpandableListView listView;
    private ExpandableListAdapter listAdapter;
    private List<String> listDataHeader;
    private HashMap<String,List<String>> listHash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_world_and_spanish_economy_concepts_gross);

        setTitle("Gross Domestic Product");

        listView = (ExpandableListView)findViewById(R.id.lvExp);
        initData();
        listAdapter = new ExpandableListAdapter(this,listDataHeader,listHash);
        listView.setAdapter(listAdapter);
    }
    private void initData() {
        listDataHeader = new ArrayList<>();
        listHash = new HashMap<>();

        listDataHeader.add("Nominal GDP");
        listDataHeader.add("Gdp per head");
        listDataHeader.add("Real GDP");
        listDataHeader.add("Sources of growth");
        listDataHeader.add("Personal consumption");
        listDataHeader.add("Investment");
        listDataHeader.add("Government spending");
        listDataHeader.add("Exports and imports");
        listDataHeader.add("Productivity");


        List<String> nominalgdp = new ArrayList<>();
        nominalgdp.add("Nominal gdp or gni (gnp) is used to measure total economic activity. The choice between the two depends largely on national conventions . Where gni/gnp is higher than gdp it indicates net investment income from abroad.");

        List<String> gdpperhead = new ArrayList<>();
        gdpperhead.add("Output per head is a good guide to living standards. It implicitly allows for qualitative factors such as literacy or health although these are not covered directly.If real gdp per head increases it indicates an improvement in overall economic well-being.");

        List<String> realgdp = new ArrayList<>();
        realgdp.add("Real (constant price) gdp or gdp figures reveal changes in economic output after adjusting for inflation. These should be put in the context of the cycle. Strong economic growth following a recession may simply indicate that slack capacity is being put back into use. Strong growth when the economy is already buoyant may indicate the installation of new capacity which will add still more to future output. However, excess growth at the top of the cycle may bubble over into inflation and/or imports. ");

        List<String> sog = new ArrayList<>();
        sog.add("Long-term growth. In the long term the growth in economic output depends on the number of people working and output per worker (productivity). Clearly there are limits to changes in the size of the population and the number of people in employment. But only an extreme pessimist can see an end to long-term productivity improvements. Output per worker grows through technical progress and investment in new plant, machinery and equipment. Investment and productivity are therefore the basis for continued and sustained economic expansion. The Productivity brief quantifies the relationship between employment, investment and growth. It is important to distinguish between economic growth, which may reflect nothing more than an expanding population, and overall economic welfare which, if measured by the volume of goods and services produced per person, improves only if output grows faster than the population");
        sog.add("Short-term cycles. The brief on Cyclical indicators explains the way that economic growth fluctuates around the trend. The brief is important because it lays the basis for interpreting many economic indicators. For example, a downturn in housing starts or an increase in inventories may signal a recession perhaps 12 months hence.");
        sog.add("The circular flow of incomes. Firms and households are the backbone of an economy. Firms employ people to make goods and provide services. This gives households their incomes. Household spending provides the rationale for the existence of firms. Thus the circular flow continues; in short, output income expenditure. There are leakages from and injections into the circular flow. Money is taken out of circulation when people buy imports, save or pay taxes. This means less spending, so firms sell fewer goods and services. Money is put into circulation when people run down their savings or borrow, when governments spend their taxes and when foreigners buy exports. These actions boost spending, so firms sell more goods and services. All leakages and injections affect spending power and influence savings and investment decisions. These may be thought of as causing cyclical variations while productivity determines long-term growth. Life is never simple, of course, and productivity depends on investment, which in turn depends on many factors including the cycle itself.");

        List<String> personalconsumption = new ArrayList<>();
        personalconsumption.add("Growth in this category often leads a general recovery from recession, encouraging manufacturers to invest more. However, if consumption grows faster than the productive capacity of an economy, imports are sucked in and inflation rises. Personal consumption typically accounts for 60% of gdp in industrial countries, so a change in consumption has a big effect on total output.");

        List<String> investment = new ArrayList<>();
        investment.add("This is a key component, contributing to current growth and laying down the foundation for future expansion. Look for spending on machinery (which produces more output) rather than, say, dwellings.");

        List<String> government = new ArrayList<>();
        government.add("This reflects, to some extent, politics rather than market forces. Its share in gdp is higher in countries where the state provides many services. A short-term increase in government spending can provide a stabilising boost to the economy, but in general it diverts resources from productive growth.");

        List<String> exportsandimports = new ArrayList<>();
        exportsandimports.add("Exports contribute to overall gdp growth; higher imports reduce the increase in output relative to the growth in demand. A sudden increase in import penetration (imports divided by gdp) suggests that consumer demand is growing faster than the domestic economy can cope with (overheating). A longer-term increase in imports relative to exports may imply a decline in the competitiveness of domestic producers. Imports that are substantially larger than exports may point to exchange-rate problems.");

        List<String> productivity = new ArrayList<>();
        productivity.add("Productivity measures the amount of output that is produced with given amounts of factor inputs (mainly land, labour and capital). Land is basically fixed and capital is very difficult to measure, so attention tends to focus on labour productivity. This can be defined in various ways. The most common are as follows. Labour productivity reflects capital investment and offers a guide to capital productivity. A company using high-tech machinery will probably produce more per person than a company using nineteenthcentury steam technology (such as in eastern Europe). Other factors which affect labour productivity include social attitudes, work ethics, unionisation and, perhaps most important, training. These are not measured directly by economic statistics.");



        listHash.put(listDataHeader.get(0),nominalgdp);
        listHash.put(listDataHeader.get(1),gdpperhead);
        listHash.put(listDataHeader.get(2),realgdp);
        listHash.put(listDataHeader.get(3),sog);
        listHash.put(listDataHeader.get(4),personalconsumption);
        listHash.put(listDataHeader.get(5),investment);
        listHash.put(listDataHeader.get(6),government);
        listHash.put(listDataHeader.get(7),exportsandimports);
        listHash.put(listDataHeader.get(8),productivity);
    }
}
