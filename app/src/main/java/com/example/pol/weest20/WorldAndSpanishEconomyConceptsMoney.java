package com.example.pol.weest20;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ExpandableListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class WorldAndSpanishEconomyConceptsMoney extends BaseActivity {

    private ExpandableListView listView;
    private ExpandableListAdapter listAdapter;
    private List<String> listDataHeader;
    private HashMap<String,List<String>> listHash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_world_and_spanish_economy_concepts_money);

        setTitle("Money And Financial Markets");

        listView = (ExpandableListView)findViewById(R.id.lvExp);
        initData();
        listAdapter = new ExpandableListAdapter(this,listDataHeader,listHash);
        listView.setAdapter(listAdapter);
    }
    private void initData() {
        listDataHeader = new ArrayList<>();
        listHash = new HashMap<>();

        listDataHeader.add("Interest rates");
        listDataHeader.add("Money supply");
        listDataHeader.add("Narrow money");
        listDataHeader.add("Money-market rates");
        listDataHeader.add("Bond yields");

        List<String> edmtDev = new ArrayList<>();
        edmtDev.add("Interest rates are the price of money. They link large stocks of physical and financial assets with smaller flows of savings and investment; they connect the present and the future; and they are sensitive to inflation expectations. As a result they are very volatile and hard to predict. With all these factors at work, it is hardly surprising that there is no simple theory which explains why interest rates behave as they do.");

        List<String> androidStudio = new ArrayList<>();
        androidStudio.add("Money is anything which is accepted as a medium of exchange; essentially currency in circulation plus bank deposits. Notes and coin, issued by the monetary authorities (mainly central banks), account for only a tiny proportion of the money supply. The rest is bank deposits which are initially created within the banking sector. The total amount of money in circulation, the money stock or money supply depending how you look at it, is often called M. The number of times it changes hands each year is its velocity of circulation, V. Multiply the two together (M V) and you have the amount of money that is spent, which by definition must equal real output Y multiplied by the price index P; that is, M V Y P. This equation is the basis for understanding money. Assume for the moment that velocity is fixed or predictable (it is not particularly). In this case, argue the monetarists, controlling the money supply controls money gdp (that is, Y P); and if the trend in real output Y can be predicted inflation can be controlled. Their opponents argue that cause and effect run in the other direction that money gdp fixes the demand for money and there is nothing that can be done about it. Whoever is right, if you are prepared to accept that velocity is fixed in the short term, then as a dangerously crude rule of thumb, subtract the inflation rate from the rate of growth of money to estimate the growth of real output.v");

        List<String> xamarin = new ArrayList<>();
        xamarin.add("M1. In most countries the measure of narrow money is called m1. This is fairly uniformly defined as currency in circulation plus sight deposits (accounts where cash is available on demand). There are some national variations. Britain’s narrow money measure is called m0. This consists almost entirely of cash in circulation, but also includes banks’ operational deposits at the Bank of England. Britain has no m1 measure. America’s m1 measure includes travellers’ cheques. Japan’s definition includes the government’s sight deposits. The number of national variations was reduced in the run-up to the creation of the euro, with the harmonisation of the definition of monetary aggregates across the euro area. The European Central Bank publishes monetary statistics for the whole euro area from figures compiled by national central banks.");
        xamarin.add("The main wider definitions of money are called m2 and m3. In essence, m2 consists of m1 plus savings deposits and time deposits (accounts where cash is available after a notice period). The definition of m3 is wider still. In America m2 consists of m1 plus savings deposits, time deposits and retail money-market mutual funds. In the euro area m2 is defined as m1 plus deposits with agreed maturity of up to two years plus deposits redeemable at up to three months’ notice. In America m3 consists of m2 plus institutional money funds, large time deposits, repurchase agreements and Eurodollars. In the euro area m3 equals m2 plus repurchase agreements, money-market funds and paper, and debt securities of up to two years’ maturity. In Japan, the measure of broad money is m2 plus certificates of deposit. m2 consists of currency in circulation plus public- and private-sector deposits.");

        List<String> uwp = new ArrayList<>();
        uwp.add("Money markets are the markets in which banks and other intermediaries trade in short-term financial instruments. The hub is usually the interbank market (called the Federal funds market in America), which is where banks deal with each other to meet their reserve requirements and, longer-term, to finance loans and investments. Very short-term interbank interest rates are largely determined by central bank intervention, although market pressures are also influential. For other maturities and other financial instruments, relative maturities and credit risks are also important.");

        List<String> asd = new ArrayList<>();
        asd.add("Bonds are loans with fixed regular coupons (interest payments) and usually a fixed redemption value on a given date. Some bonds are perpetual or undated loans which are never repaid or are repaid only at the borrower’s option. The yield (effective rate of interest) on bonds is determined by market conditions. Investors in bonds want to be compensated for loss of interest on other instruments, the time and credit risk of holding bonds and expected inflation. Risks are minimized with government bonds (loans to the government) and unlike shares the maturity value is fixed, so the yield on long-dated government bonds may be taken as an indicator of expected trends in interest rates and inflation.");

        listHash.put(listDataHeader.get(0),edmtDev);
        listHash.put(listDataHeader.get(1),androidStudio);
        listHash.put(listDataHeader.get(2),xamarin);
        listHash.put(listDataHeader.get(3),uwp);
        listHash.put(listDataHeader.get(4),asd);
    }
}
