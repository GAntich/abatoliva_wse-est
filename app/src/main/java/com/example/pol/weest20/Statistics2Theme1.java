package com.example.pol.weest20;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.widget.ExpandableListView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Statistics2Theme1 extends BaseActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics2_theme1);

        setTitle("Conceptos generales");

        TextView textview = (TextView) findViewById(R.id.textgeneralconcepts);

        Spanned text = Html.fromHtml("<b>Población</b>: conjunto completo de individuos para los cuales se desea obtener información.<br><br><b>Muestra</b>: subconjunto de individuos de la población para los cuales realmente se obtiene información de interés.<br><br><b>Fiabilidad (Precisión)</b>: La fiabilidad de una muestra está vinculada a la precisión de sus resultados, es decir, al tamaño de muestra.<br><br> <b>Validez (Sesgo)</b>: La validez de una muestra se refiere a que la muestra no presente sesgos, es decir, errores de medida sistemáticos atribuibles a otra causa distinta.<br><br><b>Experimento aleatorio</b>: es aquel experimento que bajo las mismas condiciones puede producir resultados diferentes pero con una distribución regular de resultados para un número grande de repeticiones. <br><br><b>Experimento no aleatorio</b>: es aquel experimento que bajo las mismas condiciones siempre conduce a un mismo resultado.<br><br><b>Variables aleatorias</b>: son aplicaciones que transforman los resultados de un experimento aleatorio en números con el fin de poder realizar las operaciones más usuales, luego todos los resultados de un experimento aleatorio quedan recogidos a una variable aleatoria.<br><br> <b>Un parámetro</b>: es un número que describe una característica de la población. En la practica los valores de los parámetros. <br><br><b>Un estadístico</b>: es un número que se calcula a partir de los datos de una muestra de la población. En la práctica se utilizan los estadísticos para estimar los parámetros de la población. <br><br><b>Un estimador</b>: es cualquier función de una muestra, esto es un estadístico es un estimador puntual. <br><br><b>Hipótesis Nula (H0)</b>: planteada para verificar si aceptamos o rechazamos la hipótesis después del análisis estadístico de los datos.<br><br><b>Hipótesis Alternativa (H1)</b>: es la hipótesis complementaria de la H0.<br><br><b>P-valor</b>: probabilidad que, bajo H0, el estadístico de contraste tome un valor al menos tan alejado com el realmente obtenido.<br><br><b>Pruebas paramétricas</b>: son un tipo de pruebas de significación estadística que cuantifican la asociación o independencia entre una variable cuantitativa y una categórica. Su no cumplimiento conlleva la necesidad de recurrir a pruebas estadísticas no paramédicas.<br><br><b>Pruebas no paramétricas</b>: La estadística no paramétrica es una rama de la estadística que estudia las pruebas y modelos estadísticos cuya distribución subyacente no se ajusta a los llamados criterios paramétricos." +
                "" +
                "");
        textview.setText(text);

    }

}
