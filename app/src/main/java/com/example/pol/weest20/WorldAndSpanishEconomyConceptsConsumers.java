package com.example.pol.weest20;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ExpandableListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class WorldAndSpanishEconomyConceptsConsumers extends BaseActivity {

    private ExpandableListView listView;
    private ExpandableListAdapter listAdapter;
    private List<String> listDataHeader;
    private HashMap<String,List<String>> listHash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_world_and_spanish_economy_concepts_consumers);

        setTitle("Consumers");

        listView = (ExpandableListView)findViewById(R.id.lvExp);
        initData();
        listAdapter = new ExpandableListAdapter(this,listDataHeader,listHash);
        listView.setAdapter(listAdapter);
    }
    private void initData() {
        listDataHeader = new ArrayList<>();
        listHash = new HashMap<>();

        listDataHeader.add("Personal income");
        listDataHeader.add("Real personal income ");
        listDataHeader.add("Personal and household savings");
        listDataHeader.add("Consumer confidence");
        listDataHeader.add("The cycle");
        listDataHeader.add("Consumer and personal expenditure, private consumption");




        List<String> personal = new ArrayList<>();
        personal.add("Personal income is current income received by the personal sector from all sources. The bulk is wages and salaries, but the total also covers rents (including the imputed rental value to owner-occupiers of their homes), interest and dividends (including those received by life insurance and pension funds), and current transfers such as social security benefits paid to persons and business donations to charities.");

        List<String> real = new ArrayList<>();
        real.add("Real personal income and PDI. Personal incomes and personal disposable income are occasionally quoted in nominal terms, that is, before allowing for inflation. Real personal income and real pdi are incomes adjusted for inflation. Consumer prices can be used if no other deflator is available. Loosely, if incomes rise by 5% and prices increase by 3%, real incomes are 2% higher.");

        List<String> household = new ArrayList<>();
        household.add("Personal savings, an important chunk of national savings, are personal disposable income less personal consumption. Many governments also produce savings data for households alone. The household savings ratio is household savings as a percentage of household disposable income. The household or personal sector’s financial deficit or surplus (the net balance of deposits and loans) is different from savings. For this reason consumer borrowing – which influences spending and saving – can be considered separately.");

        List<String> consumer = new ArrayList<>();
        consumer.add("Survey evidence of consumer perceptions is valuable as a leading indicator. In general, the more optimistic consumers are, the more likely they are to spend money. This boosts consumer spending and economic output. Surveys of consumer confidence are conducted by private sector organizations such as the Conference Board in America and GfK in Britain and universities such as the University of Michigan. The results are presented in index form or as percentage balances (of consumers feeling more optimistic less those feeling less optimistic).");

        List<String> cycle = new ArrayList<>();
        cycle.add("There is a cyclical pattern in consumer expenditure. The most volatile component is spending on durables: goods with a life of over one year such as washing machines, furniture and cars. When economic conditions are tight, spending on durables can be cut more readily than spending on non-durables such as food and heating. Thus there is a stable core of spending on non-durables, and a fluctuating level of spending on durables which moves in line with the economic cycle.");

        List<String> expenditure = new ArrayList<>();
        expenditure.add("Consumer expenditure is personal (mainly household) spending on goods and services. Thus it includes imputed rents on owner-occupied dwellings; the outlays which would be required to buy income in kind; and administrative costs of life insurance and pension funds. It excludes interest payments; the purchase of land and buildings; transfers abroad; all business expenditure; and spending on second-hand goods, which reflects a transfer of ownership rather than new production. Strictly speaking, expenditure takes place when goods are purchased, while consumption may take place over several years. For example, the benefit derived from a car or television is enjoyed (consumed) over several years. In practice it is hard to measure consumption and the term is used loosely to mean expenditure. Thus consumer expenditure, personal expenditure and private consumption are all the same thing.");



        listHash.put(listDataHeader.get(0),personal);
        listHash.put(listDataHeader.get(1),real);
        listHash.put(listDataHeader.get(2),household);
        listHash.put(listDataHeader.get(3),consumer);
        listHash.put(listDataHeader.get(4),cycle);
        listHash.put(listDataHeader.get(5),expenditure);
    }
}
