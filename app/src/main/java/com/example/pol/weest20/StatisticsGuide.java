package com.example.pol.weest20;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import org.w3c.dom.Text;


public class StatisticsGuide extends BaseActivity {


    public static final String EXTRA_MESSAGE = "";

    RadioButton one_sample,two_sample,more_sample,parametric,no_parametric,independent,related;
    Button search;
    RadioGroup sample,distribution,relation;
    TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics_guide);

        setTitle("Guía");

        one_sample = (RadioButton) findViewById(R.id.onesample);
        two_sample = (RadioButton) findViewById(R.id.twosample);
        more_sample = (RadioButton) findViewById(R.id.moresample);

        parametric = (RadioButton) findViewById(R.id.parametric);
        no_parametric = (RadioButton) findViewById(R.id.noparametric);

        independent = (RadioButton) findViewById(R.id.independent);
        related = (RadioButton) findViewById(R.id.related);

        sample = (RadioGroup) findViewById(R.id.group1);
        distribution = (RadioGroup) findViewById(R.id.group2);
        relation = (RadioGroup) findViewById(R.id.group3);

        title = (TextView) findViewById(R.id.textView5);

        search = (Button) findViewById(R.id.statistics_guide_search);

        sample.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(one_sample.isChecked() || more_sample.isChecked() && parametric.isChecked()){
                    independent.setEnabled(false);
                    related.setEnabled(false);
                    title.setTextColor(Color.parseColor("#9D9FA2"));
                }else {
                    independent.setEnabled(true);
                    related.setEnabled(true);
                    title.setTextColor(Color.BLACK);
                }
            }
        });

        distribution.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(more_sample.isChecked() && parametric.isChecked() || one_sample.isChecked()){
                    independent.setEnabled(false);
                    related.setEnabled(false);
                    title.setTextColor(Color.parseColor("#9D9FA2"));
                }else{
                    independent.setEnabled(true);
                    related.setEnabled(true);
                    title.setTextColor(Color.BLACK);
                }
            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(((two_sample.isChecked() || more_sample.isChecked()) && (parametric.isChecked() || no_parametric.isChecked()) && (independent.isChecked() || related.isChecked()))||((one_sample.isChecked()) && (parametric.isChecked() || no_parametric.isChecked()))||(more_sample.isChecked() && parametric.isChecked())){
                    Intent intent = new Intent(StatisticsGuide.this, StatisticsGuideSearch.class);

                    if(one_sample.isChecked() && parametric.isChecked()){
                        intent.putExtra(EXTRA_MESSAGE, "onesample_parametric_independent");
                        startActivity(intent);
                    }else if(one_sample.isChecked() && no_parametric.isChecked()) {
                        intent.putExtra(EXTRA_MESSAGE, "onesample_noparametric_independent");
                        startActivity(intent);
                    }else if(two_sample.isChecked() && parametric.isChecked() && independent.isChecked()){
                        intent.putExtra(EXTRA_MESSAGE, "twosample_parametric_independent");
                        startActivity(intent);
                    }else if(two_sample.isChecked() && parametric.isChecked() && related.isChecked()) {
                        intent.putExtra(EXTRA_MESSAGE, "twosample_parametric_related");
                        startActivity(intent);
                    }else if(two_sample.isChecked() && no_parametric.isChecked() && independent.isChecked()) {
                        intent.putExtra(EXTRA_MESSAGE, "twosample_noparametric_independent");
                        startActivity(intent);
                    }else if(two_sample.isChecked() && no_parametric.isChecked() && related.isChecked()){
                        intent.putExtra(EXTRA_MESSAGE, "twosample_noparametric_related");
                        startActivity(intent);
                    }else if(more_sample.isChecked() && parametric.isChecked()){
                        intent.putExtra(EXTRA_MESSAGE, "moresample_parametric_independent");
                        startActivity(intent);
                    }else if(more_sample.isChecked() && no_parametric.isChecked() && independent.isChecked()) {
                        intent.putExtra(EXTRA_MESSAGE, "moresample_noparametric_independent");
                        startActivity(intent);
                    }else if(more_sample.isChecked() && no_parametric.isChecked() && related.isChecked()){
                        intent.putExtra(EXTRA_MESSAGE, "moresample_noparametric_related");
                        startActivity(intent);
                    }
                }else {
                    showToast(getString(R.string.unchecked_options));
                }
            }
        });
    }
}
