package com.example.pol.weest20;

import android.os.Bundle;
import android.widget.ExpandableListView;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class WorldAndSpanishEconomyConceptsBalance extends BaseActivity {

    private ExpandableListView listView;
    private ExpandableListAdapter listAdapter;
    private List<String> listDataHeader;
    private HashMap<String,List<String>> listHash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_world_and_spanish_economy_concepts_balance);

        setTitle("The Balance of Payments");

        listView = (ExpandableListView)findViewById(R.id.lvExp);
        initData();
        listAdapter = new ExpandableListAdapter(this,listDataHeader,listHash);
        listView.setAdapter(listAdapter);
    }
    private void initData() {
        listDataHeader = new ArrayList<>();
        listHash = new HashMap<>();

        listDataHeader.add("Deficits and surpluses");
        listDataHeader.add("Trade balance");
        listDataHeader.add("Imports of goods and services");
        listDataHeader.add("Exports of goods and services");
        listDataHeader.add("Current-account balance");
        listDataHeader.add("Capital- and financial-account flows");
        listDataHeader.add("Official reserves");
        listDataHeader.add("External debt, net foreign assets");


        List<String> edmtDev = new ArrayList<>();
        edmtDev.add("The balance of payments must balance. When commentators talk about a balance of payments deficit or surplus, they mean a deficit or surplus on one part of the accounts. Attention is usually focused on the current-account balance, but capital flows have become increasingly important as many industrial countries relaxed exchange controls and other barriers to world capital flows during the 1980s. The stock of foreign direct investment more than tripled between 1990 and 2000.");

        List<String> androidStudio = new ArrayList<>();
        androidStudio.add("The trade balance is the difference between exports and imports. It may measure visible (merchandise) trade only, or trade in both goods and services. Invisibles are difficult to measure, so the balance of trade in goods and services is less reliable and more likely to be revised than the visible balance. ");

        List<String> xamarin = new ArrayList<>();
        xamarin.add("A country imports goods and services because it cannot produce them itself or because there is some comparative advantage in buying them from abroad. Some commentators worry that all imports are a drain on national resources, which is a bit like saying you should not buy from a domestic neighbour who produces something better or cheaper than you do. Of course, you can only buy to the extent that you can finance the purchase from income, savings or borrowing against future production.");

        List<String> uwp = new ArrayList<>();
        uwp.add("Exports generate foreign currency earnings. Export growth boosts gdp which in turn implies more imports, so exports should never be considered in isolation. This brief should be read in conjunction with the previous brief on imports. Note especially the comments on goods and services");

        List<String> asd = new ArrayList<>();
        asd.add("The current-account balance is the balance of trade in goods and services plus income and current transfer payments. Countries which produce monthly current-account figures base their initial estimates on simple projections of previous income and transfers, which themselves may be revised. Consequently this component of the current account is even less reliable than the goods and services balance and is subject to heavy revision.");

        List<String> asdf = new ArrayList<>();
        asdf.add("The capital and financial account records international capital and financial flows. These are frequently neglected by commentators but they are important because they are directly related to the current-account balance and to exchange rates. The increase in direct investment flows particularly reflect the increased globalization of the world economy. An outflow of capital today implies current-account income in the future. Indeed, with global deregulation, it is easier for companies to raise their market share by setting up production facilities overseas. The initial direct investment shows as a capital-account outflow. Subsequently remitted profits add to current-account inflows and boost gnp relative to gdp. The value of goods sold, however, does not show up in external trade or increase gdp in the way that exports from home would.");

        List<String> asdfg = new ArrayList<>();
        asdfg.add("Central banks hold stocks (reserves) of gold and currencies which have widespread acceptability and convertibility, such as the dollar. These reserves are used to settle international obligations and to plug temporary imbalances between supply and demand for currencies.");

        List<String> qwe = new ArrayList<>();
        qwe.add("Countries which persistently run current-account deficits accumulate external debt. This can become a major problem since essentially the debt repayments and interest can be financed only from export earnings. Net foreign assets. Rather than being debtors, many countries, including some industrial countries and oil exporters, have accumulated stocks of assets in other countries. The figures tend to understate the true position because of under-declaration and book values that are way out of line with market values. The stock, however, may not be of any help for offsetting any current-account deficits. Residents may have no wish to repatriate their assets, especially if the current-account deficit is a signal of fundamental economic problems.");

        listHash.put(listDataHeader.get(0),edmtDev);
        listHash.put(listDataHeader.get(1),androidStudio);
        listHash.put(listDataHeader.get(2),xamarin);
        listHash.put(listDataHeader.get(3),uwp);
        listHash.put(listDataHeader.get(4),asd);
        listHash.put(listDataHeader.get(5),asdf);
        listHash.put(listDataHeader.get(6),asdfg);
        listHash.put(listDataHeader.get(7),qwe);
    }
}
