package com.example.pol.weest20;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

public class StatisticsConceptsStatistics2 extends AppCompatActivity {

    GridView gridView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics_concepts_statistics2);
        setTitle("Estadística Inferencial");

        gridView = (GridView) findViewById(R.id.gridview);
        gridView.setAdapter(new GridViewImageAdapter(this, "statistics2"));

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                switch(position){
                    case 0:
                        Intent leading = new Intent(StatisticsConceptsStatistics2.this, Statistics2Theme1.class);
                        startActivity(leading);
                        break;
                    case 1:
                        Intent lagging = new Intent(StatisticsConceptsStatistics2.this, Statistics2Theme2.class);
                        startActivity(lagging);
                        break;
                    case 2:
                        Intent fiscal = new Intent(StatisticsConceptsStatistics2.this, Statistics2Theme3.class);
                        startActivity(fiscal);
                        break;
                    default:
                        break;
                }
            }
        });
    }
}
