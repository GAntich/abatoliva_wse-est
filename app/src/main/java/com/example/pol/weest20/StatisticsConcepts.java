package com.example.pol.weest20;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

public class StatisticsConcepts extends BaseActivity {

    GridView gridView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics_concepts);

        setTitle("Conceptos");

        gridView = (GridView) findViewById(R.id.gridview);
        gridView.setAdapter(new GridViewImageAdapter(this, "concepts"));

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                switch(position){
                    case 0:
                        Intent concepts = new Intent(StatisticsConcepts.this, StatisticsConceptsStatistics1.class);
                        startActivity(concepts);

                        break;
                    case 1:
                        Intent guide = new Intent(StatisticsConcepts.this, StatisticsConceptsStatistics2.class);
                        startActivity(guide);
                        break;
                    default:
                        break;
                }
            }
        });




    }
    @Override
    public void onBackPressed()
    {
        finish();
    }
}
