package com.example.pol.weest20;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class WorldAndSpanishEconomyIndicatorsLeading extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_world_and_spanish_economy_indicators_leading);

        setTitle("Leading Indicator");
    }
}
