package com.example.pol.weest20;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

public class WorldAndSpanishEconomyConcepts extends BaseActivity {

    GridView gridView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_world_and_spanish_economy_concepts);

        setTitle("Concepts");

        gridView = (GridView) findViewById(R.id.gridview);
        gridView.setAdapter(new GridViewImageAdapter(this, "econcepts"));

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                switch(position){
                    case 0:
                        Intent gdp = new Intent(WorldAndSpanishEconomyConcepts.this, WorldAndSpanishEconomyConceptsGross.class);
                        startActivity(gdp);
                        break;
                    case 1:
                        Intent cons = new Intent(WorldAndSpanishEconomyConcepts.this, WorldAndSpanishEconomyConceptsConsumers.class);
                        startActivity(cons);
                        break;
                    case 2:
                        Intent inv = new Intent(WorldAndSpanishEconomyConcepts.this, WorldAndSpanishEconomyConceptsInvestment.class);
                        startActivity(inv);
                        break;
                    case 3:
                        Intent iac = new Intent(WorldAndSpanishEconomyConcepts.this, WorldAndSpanishEconomyConceptsIndustry.class);
                        startActivity(iac);
                        break;
                    case 4:
                        Intent tbop = new Intent(WorldAndSpanishEconomyConcepts.this, WorldAndSpanishEconomyConceptsBalance.class);
                        startActivity(tbop);
                        break;
                    case 5:
                        Intent er = new Intent(WorldAndSpanishEconomyConcepts.this, WorldAndSpanishEconomyConceptsExchange.class);
                        startActivity(er);
                        break;
                    case 6:
                        Intent mafm = new Intent(WorldAndSpanishEconomyConcepts.this, WorldAndSpanishEconomyConceptsMoney.class);
                        startActivity(mafm);
                        break;
                    default:
                        break;
                }
            }
        });

    }
}
